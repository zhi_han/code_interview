#ifndef algorithms_book_hpp
#define algorithms_book_hpp

#include <utility>

/** Fibonacci number without recursion to avoid excessive evaluations*/
int fib2(int n);

/** Add two unsigned integers using their binary representation*/
int add_binary(int a, int b);

/** Multiplication with only add and substract*/
int mult_with_add(int a, int b);

/** Division with only add and subtract */
std::pair<int, int> divide_with_add(int a, int b);

#endif
