#include "algorithms_book.hpp"


#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/auto_unit_test.hpp>


BOOST_AUTO_TEST_CASE(test_fib2)
{
    std::cout << "========== Fib2 ==========" << std::endl;
    BOOST_CHECK_EQUAL(fib2(10), 34);
}


BOOST_AUTO_TEST_CASE(test_add_binary)
{
    std::cout << "========== Add binary ==========" << std::endl;
    BOOST_CHECK_EQUAL(add_binary(1,1), 2);
    BOOST_CHECK_EQUAL(add_binary(120,3), 123);
    
}

BOOST_AUTO_TEST_CASE(test_mult_with_add)
{
    std::cout << "========== Mult with add ==========" << std::endl;
    BOOST_CHECK_EQUAL(mult_with_add(1,1), 1);
    BOOST_CHECK_EQUAL(mult_with_add(5,6), 30);
}
    
BOOST_AUTO_TEST_CASE(test_divide_with_add)
{
    std::cout << "========== Devide with add ==========" << std::endl;
    std::pair<int,int> r = divide_with_add(2,2);
    BOOST_CHECK_EQUAL(r.first, 1);
    BOOST_CHECK_EQUAL(r.second, 0);
    
    std::pair<int,int> r2 = divide_with_add(11,3);
    BOOST_CHECK_EQUAL(r2.first, 3);
    BOOST_CHECK_EQUAL(r2.second, 2);

}    
