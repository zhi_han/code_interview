#ifndef circular_buffer_hpp
#define circular_buffer_hpp

#include <vector>

template<typename T>
struct CircularBuffer {
    std::vector<T> data;
    int next;
    bool filled;

    CircularBuffer(int n): data(n) {
        next = 0;
        filled = false;
    }

    void append(const T& x) {
        data[next] = x;
        next ++;
        if (next== data.size()) {
            next = 0;
            filled = true;
        }
    }
    
    T bottom() const {return data.at(next); }

};

#endif
