#ifndef chap1_hpp
#define chap1_hpp

#include <string>
#include <vector>
#include <algorithm>

/**
   1.1 Implement an algorithm to determine if a string has all unique
   characters.
 */

bool unique_char(const std::string& s);

/** 
    1.2 Write code to reverse a C-Style String.

    NOTE I did not take C-style string but rather C++ std::string.
*/

std::string reverse(const std::string& s);

/** 
    1.3 Design an algorithm and write code to remove the duplicate characters in
    a string without using any additional buffer.
*/

std::string remove_dup(const std::string& s);

/**
   1.4 Write a method to decide if two strings are anagrams or not.
 */

bool anagram(const std::string&, const std::string&);

/**
   1.5 Write a method to replace all spaces in a string with %20.
 */

std::string replace(const std::string&, char, char);

/**
   1.6 Given an image represented by an NxN matrix, where each pixel in the
   image is 4 bytes, write a method to rotate the image by 90 degrees. Can you
   do this in place?
 */

struct SquareMatrix{
    // Column major matrix
    size_t n;
    std::vector<int> data;
    
    SquareMatrix(size_t _n, const int a[]): n(_n), data() {
	for (size_t i=0; i<n*n; i++) {
	    data.push_back(a[i]);
	}
    }
    
    bool equal(const SquareMatrix& other) {
	return (n == other.n) && (data.size() == other.data.size()) && 
	    (std::equal(data.begin(), data.end(), other.data.begin()));
    }

    void print() const {
	std::cout << "[" ;
	for(size_t i=0; i< data.size(); i++) {
	    std::cout << data[i] << " ";
	}
	std::cout << "]";
    }

    void rotate();

    void zero_rows_cols();
};

/**
   1.7 Write an algorithm such that if an element in an MxN matrix is 0, its
   entire row and column is set to 0.
 */
bool isSubString(const std::string& sub, const std::string& x);

/** 1.8 Assume you have a method isSubstring which checks if one word is a
 * substring of another. Given two strings, s1 and s2, write code to check if s2
 * is a rotation of s1 using only one call to isSubstring (i.e., waterbottle
 * is a rotation of erbottlewat). */

bool isRotation(const std::string&, const std::string&);  
#endif
