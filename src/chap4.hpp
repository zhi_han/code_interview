#ifndef chap4_hpp
#define chap4_hpp

#include <string>
#include <vector>
#include <set>
#include <boost/shared_ptr.hpp>

template<typename T>
struct Node {

    boost::shared_ptr<T> e;

    boost::shared_ptr<Node> l;
    boost::shared_ptr<Node> r;

    Node(const T& x): e(new T(x)) {}
    
    template<typename Action>
    void inOrder( const Action& f) const
    {
        if (l.get() != NULL) {
            f(l.get());
        }
        f(this);
        if (r.get() != NULL) {
            f(r.get());
        }
    }

    template<typename Action>
    void preOrder( const Action& f) const
    {
        f(this);
        if (l.get() != NULL) {
            f(l.get());
        }
        if (r.get() != NULL) {
            f(r.get());
        }
    }

    template<typename Action>
    void postOrder( const Action& f) const
    {
        if (l.get() != NULL) {
            f(l.get());
        }
        if (r.get() != NULL) {
            f(r.get());
        }
        f(this);
    }

};


typedef Node<std::string> StringNode;

void printTreeIn(const StringNode& x);
void printTreePre(const StringNode& x);
void printTreePost(const StringNode& x);

template<typename Node>
struct TreeWalker {
    int maxD;
    int minD;
    
    TreeWalker(): maxD(-1), minD(-1) {}
    
    void visitNode(const Node& n, int currentD)
    {
	if ((n.l.get() == NULL) && 
	    (n.r.get() == NULL) ){
	    // leaf node
	    if (currentD > maxD) {
		maxD = currentD;
	    }
	    if (currentD < minD || minD == -1) {
		minD = currentD;
	    }
	} else {
	    if (n.l.get() != NULL) {
		visitNode(*(n.l), currentD + 1);
	    }  
	    if (n.r.get() != NULL) {
		visitNode(*(n.r), currentD + 1);
	    }
	}
    }
};


template<typename NodeType>
bool check_balance(const NodeType &n )
{
    TreeWalker<NodeType> w;
    w.visitNode(n, 0);
    return (w.maxD - w.minD <= 1);
} 


template<typename T>
struct Vertex {
    T e;
    std::vector<Vertex*> succ;
    Vertex(const T& v): e(v), succ() {}

};

template<typename VertexType>
struct GraphWalker {
    std::set<VertexType*> finished;
    VertexType* target;
    GraphWalker(VertexType* t): finished(), target(t) {};

    bool visit(VertexType* v) 
    {
	if (v==target) {
	    return true;
	}
	if (finished.find(v) != finished.end()) {
	    return false;
	}
	// std::cout << "Visit " << v->e << std::endl;   
	for(typename std::vector<VertexType*>::const_iterator it=v->succ.begin();
	    it != v->succ.end(); 
	    ++it) {
	    if (visit(*it)) {
		return true;
	    }
	}
	finished.insert(v);
	return false;
    }
};

template<typename VertexType>
bool can_reach(VertexType* from, VertexType* to)
{
    GraphWalker<VertexType> w(to);
    return w.visit(from);
}

#endif
