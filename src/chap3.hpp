#ifndef chap3_hpp
#define chap3_hpp
#include <vector>
#include "util.hpp"

template<typename T>
struct ArrayStack {
    int head;
    int tail;
    std::vector<T>& data;

    ArrayStack(std::vector<T>& d): head(0), tail(d.size()-1), data(d) {}

    void push1(const T& x) 
    {
        if (full()) {
            throw MyException("Stack is full");
        } else {
            data[head++] = x;
        }
    }

    T pop1()
    {
        if (empty1()) {
            throw MyException("Stack is empty");
        } else {
            return data[--head];
        }
    }


    void push2(const T& x) 
    {
        if (full()) {
            throw MyException("Stack is full");
        } else {
            data[tail--] = x;
        }
    }

    T pop2()
    {
        if (empty2()) {
            throw MyException("Stack is empty");
        } else {
            return data[++tail];
        }
    }


    bool full() const { return tail < head; } 

    bool empty1() { return head == 0; }
    bool empty2() { return tail == static_cast<int>(data.size()) - 1; }

    
};

struct IntStack {
    std::vector<int> data;
    std::vector<int> minIdx;

    IntStack(): data(), minIdx() {}

    void push(int x) { 
        data.push_back(x); 
        if (minIdx.size() == 0 ) {
            minIdx.push_back(0);
        } else {
            int mIdx = minIdx[minIdx.size() - 1];
            if (data[mIdx] > x) {
                // New minimum
                mIdx = data.size() -1;
                minIdx.push_back(mIdx);
            }
            
        }
    }
    int pop() {
        int result = *data.rbegin();
        data.pop_back();
        
        int mIdx = *minIdx.rbegin();
        if (static_cast<int>(data.size()) == mIdx) {
            // Popped item is a minmimum
            minIdx.pop_back();
        }
        return result;
    }
    
    int min() {
        return data.at(minIdx.at(minIdx.size() -1));
    }
};

template<typename T>
struct SetOfStack {
    typedef std::vector<T> StackOfT;

    size_t maxSize;
    int current;
    std::vector<StackOfT> stacks;
    
    SetOfStack(int m): maxSize(m), current(0) { stacks.push_back(StackOfT()); }

    void push(const T& x) {
        if (stacks[current].size() == maxSize) {
            // add a new one
            stacks.push_back(StackOfT());
            current++;
        }
        stacks[current].push_back(x);
    }

    T pop() {
        if (stacks[current].size() == 0) {
            stacks.pop_back();
            current--;
        }
        if (current < 0) {
            throw MyException("Stack is empty");
        }
        T result = stacks[current].back();
        stacks[current].pop_back();
        return result;
    }
    
};

int towers_hanoi(int n); 

void sort_stack(std::vector<int>& );

struct MyQueue {
    std::vector<int> front;
    std::vector<int> back;

    void insert(int i) { front.push_back(i); }
    int dequeue() {
	if (back.size() ==0) {
	    // Reverse front
	    int m = front.size() ;
	    for (int i = 0; i<m ; ++i) {
		back.push_back(front.back());
		front.pop_back();
	    }
	}
	int result = back.back();
	back.pop_back();
	return result;
    }
};

#endif
