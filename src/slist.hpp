#ifndef slist_hpp
#define slist_hpp

#include <boost/shared_ptr.hpp>
#include <sstream>
#include <set>
#include "util.hpp"
#include "circular_buffer.hpp"

/**
   A simple template implementing a singly linked list.

   The date member is dynamically allocated and owned by the 
   linked list.

 */
template <typename T> 
struct Cell {
    boost::shared_ptr<T> e;
    boost::shared_ptr<Cell> next;
    
    static boost::shared_ptr<Cell> nil;
    
    static boost::shared_ptr<Cell> empty() 
    {
        return nil;
    }
    
    bool is_empty() const 
    {
        return this == nil.get();
    }
    
    size_t size() const
    {
        size_t n = 0;
        const Cell* ptr = this;
        while(ptr != nil.get()) {
            n +=1;
            ptr = ptr->next.get();
        }
        return n;
    }

    static boost::shared_ptr<Cell> cons(const T& v, boost::shared_ptr<Cell> tl)
    {
        boost::shared_ptr<Cell> r(new Cell());
        r->e.reset(new T(v));
        r->next = tl;
        return(r);
    }

    void append(const T& v)
    {
        boost::shared_ptr<Cell> r(new Cell());
        r->e.reset(new T(v));
        assert(this != nil.get());
        Cell* ptr = this;
        while (ptr->next != nil) {
            ptr = ptr->next.get();
        }
        ptr->next = r;
        r->next = nil;
    }

    std::string toString() const
    {
        std::stringstream ss;
        ss << "[";
        if (this != nil.get()) {
            ss << *(this->e);
            const Cell* ptr = this->next.get();
            while (ptr != nil.get()) {
                ss << ", " << *(ptr->e);
                ptr = ptr->next.get();
            }
        }
        ss << "]";
        return ss.str();
    }
  

    T head() const 
    {
        assert(this != nil.get());
        return *(this->e);
    }

    Cell* find(const T& v) 
    {
        if (this != nil.get()) {
            Cell* ptr = this;
            while (ptr != nil.get() &&
                   *(ptr->e.get()) != v ) {
                ptr = ptr->next.get();
            }
            return ptr;
        } else {
            return nil.get();
        }
    }

    static void insert(const T& v, Cell* after)
    {
        if (after == nil.get()) {
            throw MyException("Cannot insert at Nil");
        } else {
            boost::shared_ptr<Cell> newCell(new Cell);
            newCell->e.reset(new T(v));
            newCell->next = after->next;
            after->next = newCell;
        }
    }

    /** 2.1 
        Write code to remove duplicates from an unsorted linked list. */

    void remove_dup() 
    {
        std::set<T> seen;
        if (next != nil) {
            seen.insert(*e);
            Cell* loopPtr = this;

            while (loopPtr != nil.get() && loopPtr->next != nil) {
                T myE = *(loopPtr->next->e);

                if (seen.find(myE) != seen.end()) {
                    boost::shared_ptr<Cell> toDelete = loopPtr->next;
                    loopPtr->next = toDelete->next;
                    toDelete->next.reset();
                } else {
                    seen.insert(myE);
                }
                loopPtr = loopPtr->next.get();
            }
        }
   }

    /** 2.2 Implement an algorithm to find the nth to last element of a singly *
     linked list. */
    T last_old(int n) {
        CircularBuffer<T> buffer(n);
        Cell* loopPtr = this;
        while (loopPtr != nil.get()) {
            buffer.append(*(loopPtr->e));
            loopPtr = loopPtr->next.get();
        }
        if (!buffer.filled) {
            throw MyException("Too few items");
        } else {
            return buffer.bottom();
        }
    }

    // The following is the standard solution using two pointers 
    T last(int n) {
        Cell* headPtr = this;
        Cell* tailPtr = this;
        for (int i=0; i<n; ++i) {
            headPtr = headPtr->next.get();
        }

        while (headPtr != nil.get()) {
            headPtr = headPtr->next.get();
            tailPtr = tailPtr->next.get();
        }
        return *(tailPtr->e);
    }
    
    /** 2.3 Implement an algorithm to delete a node in the middle of a single
     * linked list, given only access to that node. */
    void remove_head() {
	assert(this != nil.get());
	assert(next != nil);
	e = next->e;
	next = next->next;
    }

}; 

#endif
