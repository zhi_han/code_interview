#include "chap2.hpp"

template <>  // XXX Specialization
boost::shared_ptr<Cell<int> > Cell<int>::nil(new IntCell);

namespace {
    inline void moveNext(IntCell* &x)
    {
	if (x != IntCell::nil.get()) {
	    x = x->next.get();
	}
    
    }
    inline int get(IntCell* x){
	if (x != IntCell::nil.get()) {
	    return *(x->e);
	} else {
	    return 0;
	}
    }
}

boost::shared_ptr<IntCell> sum(const boost::shared_ptr<IntCell> a,
			       const boost::shared_ptr<IntCell> b)
{
    assert(a != IntCell::nil);
    assert(b != IntCell::nil);

    
    int first = *(a->e) + *(b->e);
    int carry = first / 10;
    first = first % 10;
    

    boost::shared_ptr<IntCell> r = IntCell::cons(first, IntCell::nil);
    IntCell* aPtr = a->next.get();
    IntCell* bPtr = b->next.get();
    while ( (aPtr != IntCell::nil.get() && carry > 0) ||
	    (bPtr != IntCell::nil.get() && carry > 0) ||
	    (aPtr != IntCell::nil.get() && bPtr != IntCell::nil.get())) {
	int ae = get(aPtr);
	int be = get(bPtr);
	int s = ae + be + carry;
	carry = s /10;
	r->append(s % 10);
	moveNext(aPtr);
	moveNext(bPtr);
    }
    return r;
}


StringRawCell* first_in_loop(StringRawCell* start)
{
    std::set<StringRawCell*> seen;
    StringRawCell* loopPtr = start;
    while (seen.find(loopPtr) == seen.end() &&
	   loopPtr != NULL) {
	seen.insert(loopPtr);
	loopPtr = loopPtr->next;
    }
    return loopPtr;
}
