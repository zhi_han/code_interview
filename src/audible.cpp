
#include "audible.hpp"
#include <vector>
#include <cstdlib>

namespace {
    class Date {
    public:
        int year;
        int month;
        int date;
        
        Date(int y, int m, int d): year(y), month(m), date(d) {}
    };

    inline Date convert_to_date(const std::string& dStr)
    {
        int year = atoi(dStr.substr(0,4).c_str());
        int month = atoi(dStr.substr(4,2).c_str());
        int date = atoi(dStr.substr(6,2).c_str());
        return Date(year, month, date);
    }


    bool later_than(const Date& d1, const Date& d2) 
    {
        // non-strict later than
        if (d1.year > d2.year) {
            return true;
        } else if (d1.year < d2.year) {
            return false;
        } else if (d1.month > d2.month) {
            return true;
        } else if (d1.month < d2.month) {
            return false;
        } else if (d1.date >= d2.date) {
            return true;
        } else {
            return false;
        }
    }
}
bool in_range(const std::vector<std::string>& start,
              const std::vector<std::string>& end, 
              const std::string& aDate)
{
    using namespace std;
    // start and end must be of same length.
    vector<string>::const_iterator sBegin = start.begin();
    vector<string>::const_iterator eBegin = end.begin();
    
    Date given = convert_to_date(aDate);
    
    for (; sBegin!= start.end(); sBegin++, eBegin++) {
        //convert input to struct
        Date start_date = convert_to_date(*sBegin);
        Date end_date = convert_to_date(*eBegin);
        if (later_than(given, start_date) && 
            later_than(end_date, given)) {
              return true;
        }
    }
    return false;    
}
