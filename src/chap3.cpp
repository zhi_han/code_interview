#include <assert.h>
#include <iostream>
#include "chap3.hpp"

struct Tower {
    std::vector<int> s1;
    std::vector<int> s2;
    std::vector<int> s3;
    
    Tower(int n): s1(), s2(), s3() 
    {
	for (int i=n; i>0; --i) {
	    s1.push_back(i);
	}       
    }

    int move(int n, 
	     std::vector<int>& from,
	     std::vector<int>& to,
	     std::vector<int>& inter)
    {
	if (n==1) {
	    assert(to.size() == 0 || from.back() < to.back());
	    int i = from.back();
	    from.pop_back();
	    to.push_back(i);
	    return 1;
	} else {
	    int steps = move(n-1, from, inter, to);
	    steps += move(1, from, to, inter);
	    steps += move(n-1, inter, to, from);
	    return steps;
	}
    }
	      

    
};

int towers_hanoi(int n)
{
    Tower hanoi(n);
    return hanoi.move(n, hanoi.s1, hanoi.s2, hanoi.s3);
}


void sort_stack(std::vector<int> & l)
{
    if (l.size() > 0) {
	int t = l.back();
	l.pop_back();

	// Sort remainder
	sort_stack(l);
	
	if (l.size() == 0) {
	    l.push_back(t);
	} else {
	    if (l.back() < t) {
		// In order
		l.push_back(t);
	    } else {
		int i = l.back();
		l.pop_back();
		l.push_back(t);
		sort_stack(l);
		l.push_back(i);
	    }
	}
    }
}
