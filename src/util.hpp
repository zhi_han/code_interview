#ifndef util_hpp
#define util_hpp
#include <string>
#include <exception>

/**
  Utility for use in the code. 
*/

/**
  My own exception type with a simple string
*/
struct MyException : public std::exception {
    std::string msg;
  public:
    ~MyException() throw () {}
    MyException(const std::string &m): msg(m) {}
    virtual char const* what() const throw()
    {
        return msg.c_str();
    }
};

#endif
