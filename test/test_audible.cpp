#include "audible.hpp"

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/auto_unit_test.hpp>


BOOST_AUTO_TEST_CASE(test_in_range)
{
    using namespace std;
    std::cout << "======Test in range======" << std::endl;
    vector<string> begin;
    vector<string> end;

    begin.push_back("19771220");
    end.push_back("20140306");

    BOOST_CHECK(in_range(begin, end, "20071229"));
    BOOST_CHECK(!in_range(begin, end, "10071229"));
}
