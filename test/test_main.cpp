#include "slist.hpp"
#include "chap1.hpp"
#include "chap2.hpp"
#include "chap3.hpp"
#include "chap4.hpp"

#include <string>
#include <iostream>
#include <vector>
#include <cstdlib>
#include <algorithm>

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/auto_unit_test.hpp>

typedef Cell<std::string> StringCell;

template <>  // XXX Specialization
boost::shared_ptr<Cell<std::string> > Cell<std::string>::nil(new StringCell);


BOOST_AUTO_TEST_CASE(test_slist)
{
  std::cout << "====Test slist====" << std::endl;
  boost::shared_ptr<StringCell> e = StringCell::empty();
  BOOST_CHECK(e->is_empty());
  BOOST_CHECK_EQUAL(e->size(), 0UL);
  std::cout << "E = " << e->toString() << std::endl;

  boost::shared_ptr<StringCell> f = StringCell::cons("first", e);
  BOOST_CHECK(!f->is_empty());
  BOOST_CHECK_EQUAL(f->size(), 1UL);
  std::cout << "F = " << f->toString() << std::endl;

  f->append("second");
  // assert(f->size() == 2UL);
  BOOST_CHECK_EQUAL(f->size(), 2UL);
  std::cout << "F = " << f->toString() << std::endl;

  StringCell* x = f->find("non");
  BOOST_CHECK(x->is_empty());
  StringCell* y = f->find("second");
  BOOST_CHECK(!y->is_empty());

  StringCell* z = f->find("second");
  StringCell::insert("third", z);
  std::cout << "F = " << f->toString() << std::endl;
  BOOST_CHECK_EQUAL(f->size(), 3UL);

}

BOOST_AUTO_TEST_CASE(test_unique_char) {
    std::cout << "====Test unique char====" << std::endl;
    BOOST_CHECK(unique_char("abcxyz"));
    BOOST_CHECK(!unique_char("unique_char"));
}

BOOST_AUTO_TEST_CASE(test_reverse) {
    std::cout << "Test reverse string" << std::endl;
    BOOST_CHECK_EQUAL(reverse("abcxyz"), "zyxcba");
}

BOOST_AUTO_TEST_CASE(test_remove_dup) {
    std::cout << "Test remove duplicate" << std::endl;
    BOOST_CHECK_EQUAL(remove_dup("abcabc"), "abc");
}

BOOST_AUTO_TEST_CASE(test_anagram) {
    std::cout << "Test anagram" << std::endl;
    BOOST_CHECK(anagram("orchestra", "carthorse"));
    BOOST_CHECK(!anagram("abcd", "abca"));
}

BOOST_AUTO_TEST_CASE(test_replace) {
    std::cout << "Test replace" << std::endl;
    BOOST_CHECK_EQUAL(replace("a  b", ' ', '\20'), "a\20\20b");
}

BOOST_AUTO_TEST_CASE(test_rotate) {
    std::cout << "Test matrix rotate" << std::endl;
    {
	int x[] = {1, 2, 3, 4};
	SquareMatrix m(2,x);
	m.rotate();
	int y[] = {3, 1, 4, 2};
	SquareMatrix n(2,y);
	BOOST_CHECK(m.equal( n));
    }
    {
	int x[] = {1, 2, 3, 4,5,6,7,8,9};
	SquareMatrix m(3,x);
	m.rotate();
	int y[] = {7,4,1,8,5,2,9,6,3};
	SquareMatrix n(3,y);
	BOOST_CHECK(m.equal( n));
    }
}

BOOST_AUTO_TEST_CASE(test_zero_rows_cols) {
    std::cout << "Test zero" << std::endl;
  {
	int x[] = {1, 0, 3, 0,5,6,7,8,9};
	SquareMatrix m(3,x);
	m.zero_rows_cols();
	int y[] = {0,0,0,0,0,0,0,0,9};
	SquareMatrix n(3,y);
	BOOST_CHECK(m.equal( n));
    }
  {
	int x[] = {1, 2, 3, 0,5,6,7,8,9};
	SquareMatrix m(3,x);
	m.zero_rows_cols();
        m.print();
	int y[] = {0,2,3,0,0,0,0,8,9};
	SquareMatrix n(3,y);
	BOOST_CHECK(m.equal( n));
  }
}

BOOST_AUTO_TEST_CASE(test_is_substring) {
    std::cout << "Is substring" << std::endl;
    BOOST_CHECK(isSubString("abc", "abc"));
    BOOST_CHECK(isSubString("abc", "xcabcb"));
    BOOST_CHECK(!isSubString("yabc", "xcabcb"));
    BOOST_CHECK(isRotation("water", "terwa"));    
    BOOST_CHECK(!isRotation("water", "terw"));    
}

BOOST_AUTO_TEST_CASE(test_slist_remove_dup) {
    std::cout << "Linked list remove dup" << std::endl;
    boost::shared_ptr<StringCell> e = StringCell::empty();
    boost::shared_ptr<StringCell> f = StringCell::cons("a", e);
    f->append("a");
    f->remove_dup();
    BOOST_CHECK_EQUAL(f->size(), 1UL);
 }

BOOST_AUTO_TEST_CASE(test_slist_last_n) {
    std::cout << "Linked list last n items" << std::endl;
    boost::shared_ptr<StringCell> e = StringCell::empty();
    boost::shared_ptr<StringCell> f = StringCell::cons("0", e);
    f->append("1");
    f->append("2");
    f->append("3");
    BOOST_CHECK_EQUAL(f->last(4), "0");
    BOOST_CHECK_EQUAL(f->last(2), "2");
}

BOOST_AUTO_TEST_CASE(test_slist_remove_head) {
    std::cout << "Remove in the middle" << std::endl;
    boost::shared_ptr<StringCell> e = StringCell::empty();
    boost::shared_ptr<StringCell> c = StringCell::cons("c", e);
    c->append("d");
    c->append("e");
    
    c->remove_head();
    std::cout << "Removed " << c->toString() << std::endl;
    BOOST_CHECK_EQUAL(c->size(), 2UL);
}

BOOST_AUTO_TEST_CASE(test_sum_list) {
    boost::shared_ptr<IntCell> x = IntCell::cons(3, IntCell::nil);
    x->append(1);
    x->append(5);

    boost::shared_ptr<IntCell> y = IntCell::cons(5, IntCell::nil);
    y->append(9);
    y->append(2);

    boost::shared_ptr<IntCell> z = sum(x, y);
    BOOST_CHECK_EQUAL(z->last(1), 8);
    BOOST_CHECK_EQUAL(z->last(2), 0);
    BOOST_CHECK_EQUAL(z->last(3), 8);
}


BOOST_AUTO_TEST_CASE(test_loop_start) {
    std::string a = "A";
    std::string b = "B";
    std::string c = "C";
    std::string d = "D";
    std::string e = "E";
    
    StringRawCell* aC = new StringRawCell(&a);
    StringRawCell* bC = new StringRawCell(&b);
    StringRawCell* cC = new StringRawCell(&c);
    StringRawCell* dC = new StringRawCell(&d);
    StringRawCell* eC = new StringRawCell(&e);
    
    aC->next = bC;
    bC->next = cC;
    cC->next = dC;
    dC->next = eC;
    eC->next = cC;

    BOOST_CHECK_EQUAL(first_in_loop(aC), cC);
    
}

BOOST_AUTO_TEST_CASE(test_two_stacks) {
    std::vector<std::string> data(2); // 2 elements
    ArrayStack<std::string> stack(data);

    stack.push1("x");
    stack.push2("y"); 

    BOOST_CHECK(stack.full());
    std::string x = stack.pop1();
    BOOST_CHECK_EQUAL(x, "x");
    stack.push2("z");
    BOOST_CHECK(stack.full());
    std::string z = stack.pop2();
    BOOST_CHECK_EQUAL(z, "z");
}

BOOST_AUTO_TEST_CASE(test_stack_min) {
    IntStack s;
    s.push(1);
    s.push(0);
    s.push(2);
    s.push(-1);

    BOOST_CHECK_EQUAL(s.min(), -1);
    int i = s.pop();
    BOOST_CHECK_EQUAL(i, -1);
    BOOST_CHECK_EQUAL(s.min(), 0);

}

BOOST_AUTO_TEST_CASE(test_stack_set) {
    SetOfStack<int> s(2);
    s.push(1);
    s.push(2);
    BOOST_CHECK_EQUAL(s.stacks.size(), 1UL);
    s.push(3);
    BOOST_CHECK_EQUAL(s.stacks.size(), 2UL);
    int x = s.pop();
    BOOST_CHECK_EQUAL(x, 3);
    int y = s.pop();
    BOOST_CHECK_EQUAL(y, 2);
    BOOST_CHECK_EQUAL(s.stacks.size(), 1UL);
}

BOOST_AUTO_TEST_CASE(test_tower_hanoi) {
    BOOST_CHECK_EQUAL(towers_hanoi(1), 1);
    BOOST_CHECK_EQUAL(towers_hanoi(2), 3);
    BOOST_CHECK_EQUAL(towers_hanoi(3), 7);
    BOOST_CHECK_EQUAL(towers_hanoi(4), 15);
    BOOST_CHECK_EQUAL(towers_hanoi(5), 31);
}

BOOST_AUTO_TEST_CASE(test_queue) {
    MyQueue q;
    q.insert(1);
    q.insert(2);
    int x = q.dequeue();
    BOOST_CHECK_EQUAL(x, 1);
    q.insert(3);
    q.insert(4);
    BOOST_CHECK_EQUAL(q.dequeue(), 2);
    BOOST_CHECK_EQUAL(q.dequeue(), 3);
    BOOST_CHECK_EQUAL(q.dequeue(), 4);

}

BOOST_AUTO_TEST_CASE(test_sort) {
    std::vector<int> a;
    for (int i=0; i<10; ++i){
	a.push_back(i);
    }
    std::random_shuffle(a.begin(), a.end());
    sort_stack(a);
    
    for(int i=0; i<0; ++i){
	BOOST_CHECK(a[i] < a[i+1]);
    }    
}


BOOST_AUTO_TEST_CASE(test_tree_visit) {
    std::cout << "Test tree visit" << std::endl;
    boost::shared_ptr<StringNode> a(new StringNode("A"));
    boost::shared_ptr<StringNode> b(new StringNode("B"));
    boost::shared_ptr<StringNode> c(new StringNode("C"));
    a->l = b;
    a->r = c;
    printTreeIn(*a);
    std::cout << std::endl;
    printTreePre(*a);
    std::cout << std::endl;
    printTreePost(*a);
    std::cout << std::endl;
}

BOOST_AUTO_TEST_CASE(test_tree_balance) {
    std::cout << "Test tree visit" << std::endl;
    boost::shared_ptr<StringNode> a(new StringNode("A"));
    boost::shared_ptr<StringNode> b(new StringNode("B"));
    boost::shared_ptr<StringNode> c(new StringNode("C"));
    a->l = b;
    a->r = c;

    BOOST_CHECK(check_balance(*a));

    boost::shared_ptr<StringNode> d(new StringNode("d"));
    boost::shared_ptr<StringNode> e(new StringNode("e"));    
    c->r = d;
    d->r = e;
    BOOST_CHECK(!check_balance(*a));
    
}

BOOST_AUTO_TEST_CASE(test_reach) {
    Vertex<std::string> a("a");
    Vertex<std::string> b("b");
    Vertex<std::string> c("c");
    Vertex<std::string> d("d");
    Vertex<std::string> e("e");
    Vertex<std::string> f("f");
    a.succ.push_back(&b);
    a.succ.push_back(&c);
    b.succ.push_back(&d);
    c.succ.push_back(&d);
    d.succ.push_back(&e);
    c.succ.push_back(&f);
    
    BOOST_CHECK(can_reach(&a, &f));
    BOOST_CHECK(!can_reach(&c, &a));
}
