#ifndef audible_hpp
#define audible_hpp

#include <vector>
#include <string>

/** Write a system that takes as input an array of start and end date
 * (format=YYYYMMDD) ranges. Create a system function that, passed in a date
 * with the same format, determines if that date falls inside any of the ranges,
 * and returns true or false. */

bool in_range(const std::vector<std::string>& begin, 
              const std::vector<std::string>& end, 
              const std::string& given);

#endif
