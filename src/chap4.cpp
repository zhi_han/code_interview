#include <iostream>
#include "chap4.hpp"

namespace {
    struct visitor {
        void operator()(const StringNode* x) const
        {
            std::cout << *(x->e) << " ";
        }
    };
}

void printTreeIn(const StringNode& n)
{
    visitor v;
    n.inOrder(v);
}
void printTreePre(const StringNode& n)
{
    visitor v;
    n.preOrder(v);
}
void printTreePost(const StringNode& n)
{
    visitor v;
    n.postOrder(v);
}
