#include "algorithms_book.hpp"
#include <vector>
#include <assert.h>
#include <iostream>

int fib2(int n)
{
    std::vector<int> f;
    for (int i=0; i<n; ++i) {
        if (i==0) {
            f.push_back(0);
        } else if (i==1) {
            f.push_back(1);
        } else {
            int k = f.at(i-1) + f.at(i-2);
            f.push_back(k);
        }
    }
    return *f.rbegin();
}


// Bit-wise addition for unsigned integer
namespace {
    std::vector<bool> convert_bit_vector(int x) 
    {
        assert(x >= 0);
        // LSB at head
        std::vector<bool> r;
        while (x != 0) {
            r.push_back(x & 1);
            x = x >> 1;
        }

        return r;
    }

    int convert_int(const std::vector<bool>& x)
    {
        using namespace std;
        int r = 0;
        for (vector<bool>::const_reverse_iterator iter = x.rbegin();
             iter != x.rend(); iter++){
            r = r << 1;
            if (*iter) {
                r = r | 1;
            }
        }
        return r;
    }

    void disp(const std::vector<bool> & x)
    {
        using namespace std;
        for (vector<bool>::const_iterator iter = x.begin();
             iter != x.end(); iter++){
            cout << *iter << " ";
        }
        cout << endl;
    }
    
    std::vector<bool> add_bit_vector(const std::vector<bool>& a,
                                     const std::vector<bool>& b)
    {
        int n = (a.size()> b.size() ? a.size() : b.size());
        bool carry = false;
        std::vector<bool> r;


        for (int i=0; i<n; ++i) {
            bool aBit = false;
            if (static_cast<int>(a.size()) > i) {
                aBit = a.at(i);
            }

            bool bBit = false;
            if (static_cast<int>(b.size()) > i) {
                bBit = b.at(i);
            }

            if (aBit && bBit && carry) {
                r.push_back(true);
                carry = true;
            } else if ((aBit && bBit && !carry) ||
                       (aBit && !bBit && carry) ||
                       (!aBit && bBit && carry)) {
                r.push_back(false);
                carry = true;
            } else if (!aBit && !bBit && !carry) {
                r.push_back(false);
                carry = false;
            } else {
                r.push_back(true);
                carry = false;
            }

        }
        // Handle top 
        if (carry) {
            r.push_back(true);
        }

        return r;
    }
}

int add_binary(int a, int b)
{
    std::vector<bool> aV = convert_bit_vector(a);
    std::vector<bool> bV = convert_bit_vector(b);
    std::vector<bool> r = add_bit_vector(aV, bV);
    return convert_int(r);
}


int mult_with_add(int a, int b)
{
    // Recursive function 
    if (b == 0) {
        return 0;
    } else {
        
        if (b & 1) {
            // b is odd
            int y = mult_with_add(a, b>>1) << 1;
            return (a + y );
        } else {
            return (mult_with_add(a, b>> 1) << 1);
        }
    }
}


std::pair<int, int> divide_with_add(int a, int b)
{
    using namespace std;
    if (b == 1) {
        return make_pair(a, 0);
    } else {
        if (a ==0) {
            return make_pair(0, 0);
        } else {
            int a_half = a >> 1;
            std::pair<int,int> result = divide_with_add(a_half, b);
            int r = result.second;
            int q = result.first;
            if (a & 1) {
                // a is odd
                r = (r << 1) | 1 ;
            } else {
                r = r << 1;
            }
            q = q << 1;
            if (r >= b) {
                r = r - b;
                q = q + 1;
            }
            return make_pair(q, r);
        }
    }
}
