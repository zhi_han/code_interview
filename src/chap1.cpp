#include <string>
#include <map>
#include <set>
#include <iostream>
#include "chap1.hpp"

namespace {
    size_t find_char(const std::string& s, char x, size_t begin, size_t end)
    {
        for (size_t i=begin; i != end; i++) {
            if (s.at(i) == x) {
                return i;
            }
        }
        return end;
    }
}

/**
   1.1 Implement an algorithm to determine if a string has all unique
   characters.
 */
bool unique_char(const std::string& s)
{
    size_t n = s.size();
    for (size_t i = 0; i<n-1; i++) {
        if (find_char(s, s[i], i+1, n) != n) {
            return false;
        } 
    }
    return true;
}

/** 
    1.2 Write code to reverse a C-Style String.

    NOTE I did not take C-style string but rather C++ std::string.
*/
std::string reverse(const std::string& s)
{
    std::string r;
    int n = static_cast<int>(s.size())-1;
    for (; n>=0; n--) {
	r.push_back(s[n]);
    }
    return r;
}

/** 
    1.3 Design an algorithm and write code to remove the duplicate characters in
    a string without using any additional buffer.
*/

namespace {
    void mark_dup(std::string& s)
    {
        int n = s.size();
        for (int i=0; i<n; i++) {
            int start = i;
            char x = s[i];
            while (start !=n ) {
                start = find_char(s, x, start+1, n);
                if (start != n) {
		s[start] = '\0';
                }
            }
        }
    }
}

std::string remove_dup(const std::string& s)
{
    std::string work(s);
    mark_dup(work);
    std::string result;
    for (size_t i=0; i<work.size(); ++i) {
	if (work[i] != '\0') {
	    result.push_back(s[i]);
	} 
    }
    return result;
}

/**
   1.4 Write a method to decide if two strings are anagrams or not.
 */

namespace {
    bool empty_map(const std::map<char,int>& m)
    {
	for (std::map<char,int>::const_iterator mIter = m.begin();
	     mIter != m.end(); ++mIter) {
	    if (mIter->second != 0) {
		return false;
	    } 
	}
	return true;
    }
    
}

bool anagram(const std::string& x, const std::string& y)
{
    if (x.size() != y.size()) return false;
    
    std::map<char,int> m;
    // Populate dictionary
    for (std::string::const_iterator xi = x.begin();
	 xi != x.end(); xi++) {
	if (m.find(*xi) == m.end()) {
	    m[*xi] = 1;
	} else {
	    m[*xi] = m.at(*xi) + 1;
	}
    }
    // Unpopulate dictionary
    for (std::string::const_iterator yi = y.begin();
	 yi != y.end(); yi++) {
	if (m.find(*yi) == m.end()) {
	    return false;
	} else {
	    m[*yi] = m.at(*yi) - 1;
	}
    }
    return empty_map(m);
}

/**
   1.5 Write a method to replace all spaces in a string with ‘%20’.
 */
std::string replace(const std::string& x, char f, char rp)
{
    std::string r(x);
    for (std::string::iterator i=r.begin(); i!= r.end(); i++) {
	if ((*i) == f) {
	    *i = rp;
	}
    }
    return r;
}

/**
   1.6 Given an image represented by an NxN matrix, where each pixel in the
   image is 4 bytes, write a method to rotate the image by 90 degrees. Can you
   do this in place?
 */
namespace {
    inline int flatIndex(int n, int i, int j) 
    {
	return (n*j + i);
    }

    void rotateElems(std::vector<int>& m, int n, int i, int j)
    {
	int last = m[flatIndex(n, j, n-1-i)];
	m[flatIndex(n, j, n-1-i)] = m[flatIndex(n,n-1-i, n-1-j)];
	m[flatIndex(n, n-1-i, n-1-j)] = m[flatIndex(n, n-1-j, i)];
	m[flatIndex(n,n-1-j,i)] = m[flatIndex(n, i, j)];
	m[flatIndex(n, i,j)] = last;
    }
} 

void SquareMatrix::rotate()
{
    // [(0,0) (0,1), ... 
    // (1,0), ...                
    //  ...                  (n-2), n-1)]
    // [(n-1,0) (n-1, 1) ... (n-1,n-1)] 
    // ... ...
    int m = (n+1)/2 ;
    for (size_t i=0; i < n/2; ++i){
	for (int j=0; j<m ;++j) {
	    rotateElems(data, n, i, j);
	}
    }
    
}


namespace {
    void zeroOut(std::vector<int>& m, int n, int i, bool isRow)
    {
        for (int j=0; j !=n; j++) {
            if (isRow) {
                m[flatIndex(n, i, j)] = 0;
            } else{
                m[flatIndex(n, j, i)] = 0;
            }
        }
    }
}

/**
   1.7 Write an algorithm such that if an element in an MxN matrix is 0, its
   entire row and column is set to 0.
 */
void SquareMatrix::zero_rows_cols()
{
    std::set<int> zeroedCols;
    for (size_t i=0; i!=n; i++){
        for(size_t j=0; j!=n; j++) {
            if (zeroedCols.find(j) == zeroedCols.end()) {
                // If already zeroed out, no need to zero out again.
                if (data[flatIndex(n,i,j)] == 0) {
                    zeroOut(data, n, i, true);
                    zeroOut(data, n, j, false);
                    zeroedCols.insert(j);
                    break;
                }
            }
        }
    }
}

/** 1.8 Assume you have a method isSubstring which checks if one word is a
 * substring of another. Given two strings, s1 and s2, write code to check if s2
 * is a rotation of s1 using only one call to isSubstring (i.e., “waterbottle”
 * is a rotation of “erbottlewat”). */

bool isSubString(const std::string& sub, const std::string& x)
{
    int n = sub.size();
    int m = x.size();
    if (m < n) {
        return false;
    } else if (m == n) {
        return sub == x;
    } else {
        for (int i=0; i!=m-n+1; i++) {
            std::string slice(x, i, n);
            if (slice == sub) {
                return true;
            }
        }
        return false;
    }
}


bool isRotation(const std::string& x, const std::string& y)
{
    if (x.size() != y.size()) {
        return false;
    } else {
        std::string twocopy(x);
        std::copy(x.begin(), x.end(), std::back_inserter(twocopy));
        return isSubString(y, twocopy);
    }
}
