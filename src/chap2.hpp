#ifndef chap2_hpp
#define chap2_hpp

#include <boost/shared_ptr.hpp>
#include <string>
#include "slist.hpp"

/**
   Code written for chapter 2 of "Cracking the code interview"

 */

typedef Cell<int> IntCell;

/** 

2.4 You have two numbers represented by a linked list, where each node contains
a single digit. The digits are stored in reverse order, such that the 1’s digit
is at the head of the list.

EXAMPLE
Input: (3 -> 1 -> 5) + (5 -> 9 -> 2)
Output: 8 -> 0 -> 8

*/
boost::shared_ptr<IntCell> sum(const boost::shared_ptr<IntCell> a,
			       const boost::shared_ptr<IntCell> b);

template<typename T>
struct RawCell {
    const T* e;
    RawCell* next;

    RawCell(const T* t): e(t), next(NULL) {}
};

typedef RawCell<std::string> StringRawCell;

/** 
    2.5 Given a circular linked list, implement an algorithm which returns node
    at the beginning of the loop.
*/

StringRawCell* first_in_loop(StringRawCell* );

#endif
